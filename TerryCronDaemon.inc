<?php
/**
 * My Custom Daemon
 */
class TerryCronDaemon extends DrushDaemon {

  /**
   * Implements DrushDaemon::executeTask().
   *
   * This gets executed once per loop iteration.
   */ 
  protected function executeTask($iteration_number) {
          
      // Do all of your task/job logic here.
      // This is a fully Drupal Bootstrapped environment meaning all your module
      // functions and really any function available to your site.
      drupal_cron_run();
	  
      // Log any output
      $this->log("run Cron :$iteration_number");
  }
}
