# t-drupal-module
Start Daemon

drush daemon start t_cron_daemon
Stop Daemon

drush daemon stop t_cron_daemon
Check the Status

drush daemon status t_cron_daemon
Show the Log

List the last 10 lines of the log file: 
drush daemon show-log t_cron_daemon
List the last N lines of the log file: 
drush daemon show-log t_cron_daemon --num_lines=N
